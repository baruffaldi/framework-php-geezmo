<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

// *** If the request become from the console, we have to take client 
//     information data on another indexes.
if ( ! empty( $_SERVER['TERM'] ) ) {
     $connection = explode( ' ', $_SERVER['SSH_CONNECTION'] );

     $client = array (
          'useragent'     => "{$_SERVER['TERM']}: {$_SERVER['_']}",
          'accept'        => '*',
          'ip'            => $connection[0],
          'output'        => 'xhtml',
          'charset'       => 'UTF-8',
          'languages'     => Zend_Registry::get( 'Zend_Locale' )->getLocaleList( ),
          'languageShort' => Zend_Registry::get( 'Zend_Locale' )->getLanguage( ),
          'languageLong'  => Zend_Registry::get( 'Zend_Locale' )->toString( ),
          'timezone'      => 'Europe/Berlin',
          'measure'       => 'meters',
          'currency'      => 'euro'
     );
}

else $client = array (
          'useragent'     => $_SERVER['HTTP_USER_AGENT'],
          'referer'       => $_SERVER['HTTP_REFERER'],
          'accept'        => $_SERVER['HTTP_ACCEPT'],
          'ip'            => $_SERVER['REMOTE_ADDR'],
          'output'        => 'xhtml',
          'charsets'      => $_SERVER['HTTP_ACCEPT_CHARSET'],
          'languages'     => Zend_Registry::get( 'Zend_Locale' )->getLocaleList( ),
          'charset'       => Zend_Registry::get( 'Zend_Locale' )->getHttpCharset( ),
          'languageShort' => Zend_Registry::get( 'Zend_Locale' )->getLanguage( ),
          'languageLong'  => Zend_Registry::get( 'Zend_Locale' )->toString( ),
          'languageSet'   => 'en-GB',
          'timezone'      => 'Europe/Berlin',
          'measure'       => 'meters',
          'currency'      => 'euro'
     );

// *** Here we put on registry the informations gathered
Zend_Registry::getInstance()->config['client'] = $client;

unset( $client );
