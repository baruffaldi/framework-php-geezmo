<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

// *** Set the default timezone on PHP environment
if ( ! is_null( Zend_Registry::getInstance()->config['application']['timezone'] ) )
     date_default_timezone_set( Zend_Registry::getInstance()->config['application']['timezone'] );

// *** If the cache is available we will set it on Zend_Date
if ( Zend_Registry::getInstance()->environment->isCacheActive )
     Zend_Date::setOptions( 
          array( 'cache' => Zend_Registry::get( Zend_Registry::getInstance()->environment->cacheHandler ) )
     );

// *** Set the main client language available language
try {
    $locale = new Zend_Locale( Zend_Locale::BROWSER );
} catch ( Zend_Locale_Exception $e ) {
    $locale = new Zend_Locale( Zend_Registry::getInstance()->config['application']['languageShort'] );
}

// *** Saving Zend_Locale and Zend_Date handlers on registry
Zend_Registry::set( 'Zend_Locale', $locale );
Zend_Registry::set( 'Zend_Date', new Zend_Date( $locale ) );

unset( $locale );
