<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

// *** Define Geezmo version
define( '__VERSION__', '0.0.3' );
define( '__GENERATOR__', "geezmo-core " . __VERSION__ );

header( 'X-Powered-By: ' . __GENERATOR__ );

/** @todo Automatic Geezmo updates check system with disable option on configuration */
/** @todo Automatic ZendFramework updates check system with disable option on configuration */

// *** If we're not on Apache environment, we set the right documentRoot
if ( empty( $_SERVER['DOCUMENT_ROOT'] ) )
	$_SERVER['DOCUMENT_ROOT'] = realpath( dirname( __FILE__ ) .
	                                      DIRECTORY_SEPARATOR . '..' . 
	                                      DIRECTORY_SEPARATOR . '..' ) . DIRECTORY_SEPARATOR . 'www' . 
	                                      DIRECTORY_SEPARATOR;

$gEnvironment = array( );

$basePath = realpath( dirname( __FILE__ ) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' )
          . DIRECTORY_SEPARATOR;

// *** Set environment folders path
$gEnvironment['path'] = array( 'root'         => $basePath,
                               'application'  => "{$basePath}application" . DIRECTORY_SEPARATOR,
                               'library'      => "{$basePath}library" . DIRECTORY_SEPARATOR,
                               'logs'         => "{$basePath}logs" . DIRECTORY_SEPARATOR,
                               'www'          => "{$basePath}www" . DIRECTORY_SEPARATOR,
                               'modules'      => "{$basePath}modules" . DIRECTORY_SEPARATOR,
                               'bootstrap'    => "{$basePath}application" . DIRECTORY_SEPARATOR . 
                                                 'bootstrap' . DIRECTORY_SEPARATOR,
                               'config'       => "{$basePath}application" . DIRECTORY_SEPARATOR . 
                                                 'config' . DIRECTORY_SEPARATOR,
                               'controllers'  => "{$basePath}application" . DIRECTORY_SEPARATOR . 
                                                 'controllers' . DIRECTORY_SEPARATOR,
                               'layouts'      => "{$basePath}application" . DIRECTORY_SEPARATOR . 
                                                 'layouts' . DIRECTORY_SEPARATOR,
                               'layoutHelper' => "{$basePath}application" . DIRECTORY_SEPARATOR . 
                                                 'layouts' . DIRECTORY_SEPARATOR . 
                                                 'helpers' . DIRECTORY_SEPARATOR,
                               'views'        => "{$basePath}application" . DIRECTORY_SEPARATOR . 
                                                 'views' . DIRECTORY_SEPARATOR );

// *** Environment type probing
if ( ! is_null ( $_SERVER['ENV'] ) )
     $gEnvironment['type'] = $_SERVER['ENV'];
else $gEnvironment['type'] = 'development';

$gEnvironment['isDev']  = ( $gEnvironment['type'] == 'development' ) ? true : false;
$gEnvironment['isTest'] = ( $gEnvironment['type'] == 'testing' )     ? true : false;
$gEnvironment['isPro']  = ( $gEnvironment['type'] == 'production' )  ? true : false;

// *** Save general default values
$gEnvironment['isCacheActive'] = false;

// *** Set the environment ( apache / console )
$gEnvironment['isConsoleRequest'] = ( empty( $_SERVER['TERM'] ) ) ? false : true;

// *** Save environment settings
Zend_Registry::getInstance()->environment = $gEnvironment;

unset( $gEnvironment );
