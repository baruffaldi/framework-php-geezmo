<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

// *** Instantiate Geezmo Debug Dumper
Zend_Registry::set( 'Geezmo_Debug', new Geezmo_Debug() );

// *** Set PHP Debug settings if we're not on production
if ( Zend_Registry::getInstance()->environment['isPro'] ) {
     error_reporting( );
     ini_set( 'display_errors', 0 );
     if ( ! isset( Zend_Registry::getInstance()->environment['debug'] ) )
          Zend_Registry::getInstance()->environment['debug'] = false;
} else {
     error_reporting( E_ALL ^ E_NOTICE );
     ini_set( 'display_errors', 1 );
     if ( ! isset( Zend_Registry::getInstance()->environment['debug'] ) )
          Zend_Registry::getInstance()->environment['debug'] = true;
}
