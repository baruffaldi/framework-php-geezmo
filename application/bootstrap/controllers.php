<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

// *** Get the configured default module ( index )
//     and the base URL if is set
$siteId  = Zend_Registry::getInstance()->config['application']['id'];

// *** Instantiate Front-End Controller
Zend_Registry::set( 'Zend_Controller_Front', Zend_Controller_Front::getInstance( ) );

if ( ! is_null( $siteId ) && $siteId != 'geezmo' )
{            
	Zend_Registry::set( 'Zend_Controller_Router_Route', Zend_Registry::get( 'Zend_Controller_Front' )->getRouter( ) );
	
	Zend_Registry::get( 'Zend_Controller_Router_Route' )->addRoute(
	    'default',
	    new Zend_Controller_Router_Route( ':module/:controller/:action/*',
	         array( 'module' => Zend_Registry::getInstance()->config['application']['id'], 'controller' => 'index', 'action' => 'index' ) )
	);
	
	Zend_Registry::get( 'Zend_Controller_Front' )

                  // *** Set the controllers path
	             ->setControllerDirectory( Zend_Registry::getInstance()->environment['path']['controllers'] )
	
	             // *** Set environment type as global view parameter
	             ->setParam( 'env', Zend_Registry::get( 'environment' )->type )
	             
	             // *** Add default module controllers, defined on application.ini
	             ->addControllerDirectory( Zend_Registry::getInstance()->environment['path']['modules'] . 
	                                       Zend_Registry::getInstance()->config['application']['id'] . 
	                                       DIRECTORY_SEPARATOR . 'controllers',
	                                  Zend_Registry::getInstance()->config['application']['id'] )

	             // *** Set the modules directory
	             ->addModuleDirectory( Zend_Registry::getInstance()->environment['path']['modules'] )
	             
	             // *** Set the default module
	             ->setDefaultModule( Zend_Registry::getInstance()->config['application']['id'] )
	             
                  ->addControllerDirectory( Zend_Registry::getInstance()->environment['path']['controllers'],
                                       'geezmo' )
                                       
	             ->setRouter( Zend_Registry::get( 'Zend_Controller_Router_Route' ) );
	
} else {

	Zend_Registry::set( 'Zend_Controller_Router_Route', Zend_Registry::get( 'Zend_Controller_Front' )->getRouter( ) );
	
	Zend_Registry::get( 'Zend_Controller_Router_Route' )->addRoute(
	    'default',
	    new Zend_Controller_Router_Route( ':module/:controller/:action/*',
	    array( 'module' => 'geezmo', 'controller' => 'index', 'action' => 'index' ) )
	);
	
	Zend_Registry::get( 'Zend_Controller_Front' )

	             // *** Set the controllers path
	             ->setControllerDirectory( Zend_Registry::getInstance()->environment['path']['controllers'],
	                                       'geezmo' )
	
	             // *** Set environment type as global view parameter
	             ->setParam( 'env', Zend_Registry::get( 'environment' )->type )
	             
	             // *** Set the default module
	             ->setDefaultModule( 'geezmo' )
	             
	             ->setRouter( Zend_Registry::get( 'Zend_Controller_Router_Route' ) );
}

unset( $siteId );
