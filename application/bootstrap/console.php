<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

// *** If the request become from the console, we have to process all arguments
//     and use it to route the request on the right module, controller and then
//     action. 
//     In addiction, we give the user the chance to print some useful debug data
if ( ! empty( $_SERVER['TERM'] ) )
{
	$_GET = array( );
	$_POST = array( );
	
	// *** Instantiate the arguments handler
	$geezmoConsole = new Geezmo_Zend_Console( );
	
	// *** Save it on the registry
	Zend_Registry::set( 'Geezmo_Zend_Console', $geezmoConsole );
     Zend_Registry::set( 'Zend_Console_Getopt', $geezmoConsole->getZendConsoleGetopt( ) );

	try {
		
          // *** Here we tell to the arguments handler that it got to process
          //     all arguments specified, and for each helper action, we print
          //     out data on screen and we set the no dispatch mode flag on
		Zend_Registry::get( 'Geezmo_Zend_Console' )->catchOptions( );

		// *** Print out on screen geezmo header if not specified else
		if ( ! Zend_Registry::get( 'Zend_Console_Getopt' )->getOption( 'quite' ) )
	     	Zend_Registry::get( 'Geezmo_Zend_Console' )->printHeader();
	
		if ( Zend_Registry::get( 'Zend_Console_Getopt' )->getOption( 'help' ) ) {
	     	print Zend_Registry::get( 'Zend_Console_Getopt' )->getUsageMessage();
		     define( '__NO_DISPATCH__', true );
		}
	
		if ( Zend_Registry::get( 'Zend_Console_Getopt' )->getOption( 'registry' ) ) {
	     	Zend_Registry::get( 'Geezmo_Zend_Console' )->printRegistry();
		     define( '__NO_DISPATCH__', true );
		}
	
		if ( Zend_Registry::get( 'Zend_Console_Getopt' )->getOption( 'application-settings' ) ) {
	     	Zend_Registry::get( 'Geezmo_Zend_Console' )->printSettings( 'application' );
		     define( '__NO_DISPATCH__', true );
		}
	
		if ( Zend_Registry::get( 'Zend_Console_Getopt' )->getOption( 'client-settings' ) ) {
	     	Zend_Registry::get( 'Geezmo_Zend_Console' )->printSettings( 'client' );
		     define( '__NO_DISPATCH__', true );
		}
	
		if ( Zend_Registry::get( 'Zend_Console_Getopt' )->getOption( 'geezmo-settings' ) ) {
			Zend_Registry::get( 'Geezmo_Zend_Console' )->printSettings( 'geezmo' );
		     define( '__NO_DISPATCH__', true );
		}
     	     
     // *** If the specified arguments are invalid, we have to print out the usage
	} catch ( Zend_Console_Getopt_Exception $e ) {
		
		// *** Print out on screen geezmo header
		Zend_Registry::get( 'Geezmo_Zend_Console' )->printHeader( );
		
		Zend_Registry::get( 'Geezmo_Zend_Console' )->printException( $e );

          // *** Because there is an error, here we set the no dispatch mode flag on
		define( '__NO_DISPATCH__', true );
	}
	
	unset( $geezmoConsole );
}
