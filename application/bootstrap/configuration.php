<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

$configFolder = Zend_Registry::getInstance()->environment['path']['config'];

// *** Get the application settings
if ( file_exists( "{$configFolder}application.ini" ) ) 
     $confs['application'] = new Zend_Config_Ini( 
          "{$configFolder}application.ini",
          Zend_Registry::getInstance()->environment['type'],
          array( 'allowModifications' => false ) 
     );
     
// *** If the ini version is not present, we just take the xml     
elseif ( file_exists( "{$configFolder}application.xml" ) ) 
     $confs['application'] = new Zend_Config_Xml( 
          "{$configFolder}application.xml",
          Zend_Registry::getInstance()->environment['type'],
          array( 'allowModifications' => false ) 
     );

// *** Here we scan the geezmo config folder for other settings
foreach ( glob( "$configFolder*.ini" ) as $configFile )
{
	$configName = preg_replace( '/.ini/', NULL, basename( $configFile ) );
	
	if ( $configName != 'application' )
        $confs[$configName] = new Zend_Config_Ini( 
             $configFile,
             Zend_Registry::getInstance()->environment['type'],
             array( 'allowModifications' => false ) 
        );

     $confs[$configName] = $confs[$configName]->toArray( );
}

// *** If application id it's not set we have to set it!
if ( is_null( $confs['application']['id'] ) )
     $confs['application']['id'] = 'geezmo';

// *** If is set GET variable: format, we have to change the output format
if ( ! empty( $_GET['format'] ) )
     $confs['application']['format'] = $_GET['format'];

// *** Now, we save all settings to the registry
Zend_Registry::set( 'config', $confs );

// *** Little apache server_admin variable override
$_SERVER['SERVER_ADMIN'] = $confs['application']->admin;
    
unset( $configFolder, $configFile, $configName, $confs );

/**
 * @todo Gotta write a configuration model with default 
 *       values, and then replace values by config probes.
 */
