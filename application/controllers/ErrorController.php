<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit
 * @package geezmo
 * @subpackage core
 */

class ErrorController extends Zend_Controller_Action 
{
	public function init( )
	{
		$this->_helper->getHelper( 'contextSwitch' )->initContext();
	}
	
     public function indexAction( ) { $this->_forward( 'error', 'error', $this->_getAllParams( ) ); }
     
     public function errorAction( )
     { 
          // *** Ensure the default view suffix is used so we always return good 
          //     content
          $this->_helper->viewRenderer->setViewSuffix( 'phtml' );

          // *** Grab the error object from the request
          $errors = $this->_getParam( 'error_handler' ); 

          // *** Clear previous content
          $this->getResponse()->clearBody();
          
          // *** Send the correct status code
          switch ( $errors->type ) 
          {
               case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
               case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                    $this->getResponse()->setRawHeader( 'HTTP/1.1 404 Not Found' );
                    break;
                    
               default:
               	$this->getResponse()->setRawHeader( 'HTTP/1.1 500 Internal Error' );
                    break;
          }

          // *** pass the environment to the view script so we can conditionally 
          //      display more/less information
          $this->view->env        = $this->getInvokeArg( 'env' ); 
          
          // *** pass the actual exception object to the view
          $this->view->exception = $errors->exception; 
          
          // *** pass the request to the view
          $this->view->request   = $errors->request;
      }
}