<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit
 * @package geezmo
 * @subpackage core
 */

class UserSettingsController extends Zend_Controller_Action 
{
	public function indexAction( ) { }
    
     public function setAction( )
     {
          $fileConf = Zend_Registry::getInstance()->environment['path']['config'] . 'application.ini';
          $params   = $this->_getAllParams();
          $this->view->results = array( );

          foreach ( $params as $param => $value )
               if ( $param != 'module' &&
                    $param != 'controller' &&
                    $param != 'action' )
                    $this->view->results[$param] = array ( 
                         'value' => $value, 
                         'result' => Geezmo_User_Config::setOption( $param, $value ) 
                    );
     }
}