<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit
 * @package geezmo
 * @subpackage core
 */

$libraryPath   = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 
                 '..' . DIRECTORY_SEPARATOR . 'library';

$bootstrapPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 
                 'bootstrap' . DIRECTORY_SEPARATOR;

// *** Check if is ZendFramework built-in on PHP or we have to set
//     a new include path to reach it
if ( ! file_exists( 'Zend' . DIRECTORY_SEPARATOR . 'Version.php' ) )
     set_include_path( get_include_path( ) . PATH_SEPARATOR . $libraryPath );

require_once 'Zend' . DIRECTORY_SEPARATOR . '/Loader.php';

// *** Enable the dynamic class loader based on necessity
Zend_Loader::registerAutoload( );

// *** Open Geezmo Registry
require_once "{$bootstrapPath}registry.php";

// *** Let Geezmo Bootstrap
require_once "{$bootstrapPath}geezmo.php";

// *** Server Configuration
Geezmo_Bootstrap::loadComponent( 'configuration' );

// *** Controllers
Geezmo_Bootstrap::loadComponent( 'controllers' );

// *** Views Handler
Geezmo_Bootstrap::loadComponent( 'views' );

// *** Debug Handlers
Geezmo_Bootstrap::loadComponent( 'debug' );

// *** Localization and Internalization
Geezmo_Bootstrap::loadComponent( 'g11n' );

// *** Client Configuration
Geezmo_Bootstrap::loadComponent( 'client' );

// *** Console Compatibility
Geezmo_Bootstrap::loadComponent( 'console' );

// *** Bootstrap Requested Module
Geezmo_Bootstrap::loadModules( );

unset( $libraryPath );
