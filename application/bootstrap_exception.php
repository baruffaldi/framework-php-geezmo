<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . '..'
           . DIRECTORY_SEPARATOR . 'application' 
           . DIRECTORY_SEPARATOR . 'bootstrap' 
           . DIRECTORY_SEPARATOR . 'console.php';

$o = "An exception occured while bootstrapping the application.\n\n";

// *** And if is not production environment, we print the error details
if ( ! Zend_Registry::get( 'environment' )->isPro ) 
   $o .= $e->getMessage( ) . "\n\n"
      .  "Stack Trace:\n[[[start[\n"
      .  $e->getTraceAsString( ) . "\n]end]]]";

// *** Last but not least, if HTML output is needed 
// *** we convert the string properly.
if ( ! is_null( Zend_Registry::getInstance()->config['application']['format'] ) )
    switch ( Zend_Registry::getInstance()->config['application']['format'] )
    {
    	case 'html':
    	case 'xhtml':
             $o = '<html><body>'
                . str_replace( array( '[[[start[', ']end]]]', "\n" ), 
                               array( '<pre>', '</pre>', '<br />' ),
                               htmlentities( $o ) )
                . '</body></html>';
    		break;
		
    	case 'xml':
             $o = '<?xml version="1.0" ?><error>'
                . str_replace( array( '[[[start[', ']end]]]', 
                                      'An exception', 'Stack Trace:' ), 
                               array( '<stacktrace>', '</stacktrace>', 
                                      '<text>An exception', '</text>' ), 
                               $o )
                . '</error>';
    		break;
    }

print <<<ERROR
$o
ERROR;

define( '__NO_DISPATCH__', true );

unset( $o );
