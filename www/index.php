<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

// *** Force environment setting
//$_SERVER['ENV'] = 'production';

// *** Save default PHP environment constants
$defaultConstants = get_defined_constants();

// *** Try to bootstrap the application
try { require dirname( __FILE__ ) . DIRECTORY_SEPARATOR . '..'
            . DIRECTORY_SEPARATOR . 'application'
            . DIRECTORY_SEPARATOR . 'bootstrap.php'; }

// *** If something goes bad, we handle that fatal error
catch ( Exception $e ) {
	require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . '..'
	           . DIRECTORY_SEPARATOR . 'application' 
	           . DIRECTORY_SEPARATOR . 'bootstrap_exception.php'; 
}

// *** Save constants before dispatch
Zend_Registry::set( 'Geezmo_Pre_Constants', array_diff( get_defined_constants(), $defaultConstants ) );

// *** Otherwise we dispatch the front controller
if ( ! defined( '__NO_DISPATCH__' ) )
     Zend_Registry::get( 'Zend_Controller_Front' )->dispatch();

// *** Save constants after dispatch
Zend_Registry::set( 'Geezmo_Post_Constants', array_diff( get_defined_constants(), $defaultConstants ) );

if ( Zend_Registry::getInstance()->environment['isConsoleRequest'] )
     print "\n";

unset( $defaultConstants );
