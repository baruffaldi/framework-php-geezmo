<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

class Geezmo_Console_Color
{
     public static $_colors = array (
          'LIGHT_RED'      => "[1;31m",
          'LIGHT_GREEN'    => "[1;32m",
          'YELLOW'         => "[1;33m",
          'LIGHT_BLUE'     => "[1;34m",
          'MAGENTA'        => "[1;35m",
          'LIGHT_CYAN'     => "[1;36m",
          'WHITE'          => "[1;37m",
          'NOCOLOR'        => "[0m",
          'BLACK'          => "[0;30m",
          'RED'            => "[0;31m",
          'GREEN'          => "[0;32m",
          'BROWN'          => "[0;33m",
          'BLUE'           => "[0;34m",
          'CYAN'           => "[0;36m",
          'BOLD'           => "[1m",
          'UNDERSCORE'     => "[4m",
          'REVERSE'        => "[7m"
     );

     public function colorize ( $text, $color = "NOCOLOR", $back = true )
     {
          $out = self::$_colors[$color];

          if ( empty( $out ) ) $out = "[0m";

          if ( $back )
               return chr(27)."$out$text".chr(27)."[0m";
          else
               echo chr(27)."$out$text".chr(27).chr(27)."[0m";
     }
}