<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

class Geezmo_Zend_Console
{
     private $zendConsoleGetopt = null;

     private $requestUri        = null;

     private $module            = null;
     private $controller        = null;
     private $action            = null;

     private $verboseOutput     = false;
     private $coloredTerminal   = false;

     private $argsTemplate      = array (
          'env|e-s'                => 'Set the environment (development|testing|production) (default:development)',
          'request-uri|u-s'        => 'Set the request uri',
          'module|m-s'             => 'Set the module',
          'controller|c-s'         => 'Set the controller',
          'action|a-s'             => 'Set the action',
          'get|g-s'                => 'Set the GET data',
          'post|p-s'               => 'Set the POST data',
          'output|o-s'             => 'Set the output mode (text|xml|(xhtml|html)|json|serialized) (default:text)',
          'debug|d'                => 'Enable the debug mode',
          'quite|q'                => 'Enable quite output, incompatible with -v option',
          'verbose|v'              => 'Enable verbose output',
          'no-colors|n'            => 'Disable colors on output',
          'registry|r'             => 'Display registry elements',
          'geezmo-settings|G'      => 'Display geezmo settings',
          'application-settings|A' => 'Display application settings',
          'client-settings|C'      => 'Display client settings',
          'help|h|H'               => 'Display this help'
     );

     public function __construct()
     {
          $this->module     = Zend_Registry::get( 'Zend_Controller_Front' )->getDefaultModule();
          $this->controller = Zend_Registry::get( 'Zend_Controller_Front' )->getDefaultControllerName();
          $this->action     = Zend_Registry::get( 'Zend_Controller_Front' )->getDefaultAction();

          $this->argsTemplate['module|m-s']     .= " (default: {$this->module})";
          $this->argsTemplate['controller|c-s'] .= " (default: {$this->controller})";
          $this->argsTemplate['action|a-s']     .= " (default: {$this->action})";

          $this->zendConsoleGetopt = new Zend_Console_Getopt( $this->argsTemplate );
          
          Zend_Registry::getInstance()->environment['debug']            = false;
          Zend_Registry::getInstance()->config['application']['format'] = 'text';
          
          return $this;
     }

     public function catchOptions ()
     {
          $this->zendConsoleGetopt->parse();

          if ( ! $this->zendConsoleGetopt->getOption( 'no-colors' ) )
               $this->coloredTerminal = true;

          if ( $this->zendConsoleGetopt->getOption( 'verbose' ) )
               $this->verboseOutput = true;

          if ( $this->zendConsoleGetopt->getOption( 'env' ) )
          {
               Zend_Registry::getInstance()->environment['type'] = $this->zendConsoleGetopt->getOption( 'env' );
               
               Zend_Registry::getInstance()->environment['isDev'] = 
                    ( Zend_Registry::getInstance()->environment['type'] == 'development' ) 
                    ? true : false;
                    
               Zend_Registry::getInstance()->environment['isTest'] = 
                    ( Zend_Registry::getInstance()->environment['type'] == 'testing' ) 
                    ? true : false;
                    
               Zend_Registry::getInstance()->environment['isPro'] = 
                    ( Zend_Registry::getInstance()->environment['type'] == 'production' ) 
                    ? true : false;
          }

          if ( $this->zendConsoleGetopt->getOption( 'debug' ) )
               Zend_Registry::getInstance()->environment['debug'] = true;

          if ( $this->zendConsoleGetopt->getOption( 'output' ) )
               Zend_Registry::getInstance()->config['application']['format'] = $this->zendConsoleGetopt->getOption( 'output' );

          if ( $this->zendConsoleGetopt->getOption( 'request-uri' ) )
               $this->requestUri = $this->zendConsoleGetopt->getOption( 'request-uri' );
          else 
          {
               if ( $this->zendConsoleGetopt->getOption( 'module' ) )
                    $this->module = $this->zendConsoleGetopt->getOption( 'module' );

               if ( $this->zendConsoleGetopt->getOption( 'controller' ) )
                    $this->controller = $this->zendConsoleGetopt->getOption( 'controller' );

               if ( $this->zendConsoleGetopt->getOption( 'action' ) )
                    $this->action = $this->zendConsoleGetopt->getOption( 'action' );

               $this->requestUri = "/{$this->module}/{$this->controller}/{$this->action}";
          }

          if ( $this->zendConsoleGetopt->getOption( 'get' ) )
          {
               $variables = explode( '&', $this->zendConsoleGetopt->getOption( 'get' ) );

               foreach ( $variables as $var )
               {
                    $varArray = explode( '=', $var );
                    $_GET[$varArray[0]] = urldecode( $varArray[1] );
               }

               $_SERVER['QUERY_STRING'] = $this->zendConsoleGetopt->getOption( 'get' );
               $this->requestUri .= "?" . $this->zendConsoleGetopt->getOption( 'get' );
          }

          if ( $this->zendConsoleGetopt->getOption( 'post' ) )
          {
               $variables = explode( '&', $this->zendConsoleGetopt->getOption( 'post' ) );

               foreach ( $variables as $var )
               {
                    $varArray = explode( '=', $var );
                    $_POST[$varArray[0]] = urldecode( $varArray[1] );
               }
          }

          $_SERVER['REQUEST_URI'] = $this->requestUri;
     }

     public function printSettings ( $env )
     {
     	switch ( $env )
     	{
     		case 'geezmo':
		          $ret .= ( $this->coloredTerminal )
		               ? Geezmo_Console_Color::colorize( "[", "GREEN" )
		               . Geezmo_Console_Color::colorize( "GEEZMO_SETTINGS", "LIGHT_CYAN" )
		               . Geezmo_Console_Color::colorize( "] ", "GREEN" )
		               . "\n"
		
		               : "[GEEZMO_SETTINGS] \n";
		               
     			foreach ( Zend_Registry::get( 'environment' ) as $setting => $value )
     			{
     				$value = Geezmo_Debug::exportVar( $value, true, true );
			          $ret .= ( $this->coloredTerminal )
			               ? Geezmo_Console_Color::colorize( "[", "GREEN" )
			               . Geezmo_Console_Color::colorize( "$setting", "LIGHT_GREEN" )
			               . Geezmo_Console_Color::colorize( "] ", "GREEN" )
			               . $value . "\n"
			
			               : "[$setting] $value\n";
     			}
     			
			     echo "$ret\n";
     			break;
     			
     		case 'client':
     		case 'application':
		          $ret .= ( $this->coloredTerminal )
		               ? Geezmo_Console_Color::colorize( "[", "GREEN" )
		               . Geezmo_Console_Color::colorize( strtoupper( $env ) . "_SETTINGS", "LIGHT_CYAN" )
		               . Geezmo_Console_Color::colorize( "] ", "GREEN" )
		               . "\n"
		
		               : strtoupper( $env ) . "_SETTINGS] \n";

     			foreach ( Zend_Registry::getInstance()->config[$env] as $setting => $value )
     			{
     				$value = Geezmo_Debug::exportVar( $value, true, true );
			          $ret .= ( $this->coloredTerminal )
			               ? Geezmo_Console_Color::colorize( "[", "GREEN" )
			               . Geezmo_Console_Color::colorize( "$setting", "LIGHT_GREEN" )
			               . Geezmo_Console_Color::colorize( "] ", "GREEN" )
			               . $value . "\n"
			
			               : "[$setting] $value\n";
     			}
     			
			     echo "$ret\n";
     			break;
     	}
     }

     public function printRegistry()
     {
          $ret .= ( $this->coloredTerminal )
               ? Geezmo_Console_Color::colorize( "[", "GREEN" )
               . Geezmo_Console_Color::colorize( "REGISTRY_ELEMENTS", "LIGHT_CYAN" )
               . Geezmo_Console_Color::colorize( "] ", "GREEN" )

               : "[REGISTRY_ELEMENTS] ";

          foreach ( Zend_Registry::getInstance() as $name => $content )
               $ret .= ( $this->coloredTerminal )
                    ? "$name"
                    . Geezmo_Console_Color::colorize( ", ", "GREEN" )

                    : "$name, ";

          echo "$ret\n";
     }

     public function printHeader()
     {
          echo ( $this->coloredTerminal )
               ? Geezmo_Console_Color::colorize( "--------------------------\n", "GREEN" )
               : "--------------------------\n";

          echo ( $this->coloredTerminal )
               ? Geezmo_Console_Color::colorize( "[", "GREEN" )
               . Geezmo_Console_Color::colorize( Zend_Registry::getInstance()->config['application']['version'], "MAGENTA" )
               . Geezmo_Console_Color::colorize( "] ", "GREEN" )
               . Zend_Registry::getInstance()->config['application']['name'] . "\n"

               : "[" . Zend_Registry::getInstance()->config['application']['version']
               . "] " . Zend_Registry::getInstance()->config['application']['name'] . "\n";

          echo ( $this->coloredTerminal )
               ? Geezmo_Console_Color::colorize( "[", "GREEN" )
               . Geezmo_Console_Color::colorize( __VERSION__, "MAGENTA" )
               . Geezmo_Console_Color::colorize( "] ", "GREEN" )
               . "Geezmo powered\n"

               : "[" . __VERSION__ . "] Geezmo-core powered\n";

          if ( $this->verboseOutput )
               $this->printVerboseData();

          echo ( $this->coloredTerminal )
               ? Geezmo_Console_Color::colorize( "--------------------------\n", "GREEN" )
               : "--------------------------\n";
     }

     public function printException( Exception $e )
     {
          echo $e->getMessage();

          echo ( $this->coloredTerminal )
               ? Geezmo_Console_Color::colorize( "\n--------------------------\n", "GREEN" )
               : "\n--------------------------\n";

          echo $e->getUsageMessage();
     }

     public function printVerboseData()
     {
          echo ( $this->coloredTerminal )
               ? Geezmo_Console_Color::colorize( "--------------------------\n", "GREEN" )
               : "--------------------------\n";

          echo ( $this->coloredTerminal )
               ? Geezmo_Console_Color::colorize( "[", "GREEN" )
               . Geezmo_Console_Color::colorize( "SERVER", "LIGHT_CYAN" )
               . Geezmo_Console_Color::colorize( "]\n", "GREEN" )

               : "[SERVER]\n";

          foreach ( $_SERVER as $key => $value )
               if ( $this->coloredTerminal )
                    echo Geezmo_Console_Color::colorize( "[", "GREEN" )
                       . Geezmo_Console_Color::colorize( $key, "BROWN" )
                       . Geezmo_Console_Color::colorize( "] ", "GREEN" )
                       . "$value\n";
               else
                    echo "[$key] $value\n";

          echo ( $this->coloredTerminal )
               ? Geezmo_Console_Color::colorize( "[", "GREEN" )
               . Geezmo_Console_Color::colorize( "GET", "LIGHT_CYAN" )
               . Geezmo_Console_Color::colorize( "]\n", "GREEN" )

               : "[GET]\n";

          foreach ( $_GET as $key => $value )
               if ( $this->coloredTerminal )
                    echo Geezmo_Console_Color::colorize( "[", "GREEN" )
                       . Geezmo_Console_Color::colorize( $key, "LIGHT_RED" )
                       . Geezmo_Console_Color::colorize( "] ", "GREEN" )
                       . "$value\n";
               else
                    echo "[$key] $value\n";

          echo ( $this->coloredTerminal )
               ? Geezmo_Console_Color::colorize( "[", "GREEN" )
               . Geezmo_Console_Color::colorize( "POST", "LIGHT_CYAN" )
               . Geezmo_Console_Color::colorize( "]\n", "GREEN" )

               : "[POST]\n";

          foreach ( $_POST as $key => $value )
               if ( $this->coloredTerminal )
                    echo Geezmo_Console_Color::colorize( "[", "GREEN" )
                       . Geezmo_Console_Color::colorize( $key, "RED" )
                       . Geezmo_Console_Color::colorize( "] ", "GREEN" )
                       . "$value\n";
               else
                    echo "[$key] $value\n";
     }

     public function getZendConsoleGetopt ()
     {
     	return $this->zendConsoleGetopt;
     }

     public function setZendConsoleGetopt ( Zend_Console_Getopt $resource )
     {
     	$this->zendConsoleGetopt = $resource;
     }
}