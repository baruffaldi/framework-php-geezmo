<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

class Geezmo_Config
{
	private $env;
	
	private $configFile;
	
	private $srcHandler;
	
	private $iniHandler;
	private $xmlHandler;
	
	public function __construct( )
	{
		$this->env        = Zend_Registry::getInstance()->environment['type'];
	     $this->configFile = Zend_Registry::getInstance()->environment['path']['config'] . 'application';
	     
		if ( ! file_exists( dirname( "{$this->configFile}.ini" ) ) ) return -1;
		
		if ( ! is_readable( dirname( "{$this->configFile}.ini" ) ) ) return -2;
		
		if ( ! is_writable( "{$this->configFile}.ini" ) && 
		     ! is_writable( "{$this->configFile}.xml" ) ) return -3;

		$this->srcHandler = new Zend_Config_Ini( "{$this->configFile}.ini", null );
	}
	
	public function setOption ( $param, $value = NULL )
	{
		 if ( ! is_null( $param ) )
		 {
              if ( is_null( $value ) )
                   unset( $this->srcHandler->{$env}->{$param} );
              else $this->srcHandler->{$env}->{$param} = $value;
              
              return true;
				
		} else return false;
	}
	
	public function saveConfiguration ( )
	{
              
		$this->iniHandler = new Zend_Config_Writer_Ini( array( 'skipExtends' => true,
                                                                 'config'      => $this->srcHandler,
                                                                 'filename'    => "{$this->configFile}.ini") );

		$this->xmlHandler = new Zend_Config_Writer_Xml( array( 'skipExtends' => true,
                                                                 'config'      => $this->srcHandler,
                                                                 'filename'    => "{$this->configFile}.xml") );
		
		$this->iniHandler->write( );
		$this->xmlHandler->write( );
	}
}