<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit <copyright@coolminds.org>
 * @package geezmo
 * @subpackage core
 */

class Geezmo_Debug extends phpDump
{
	public function exportVar ( $var, $oneLined = false, $return = false )
	{
		$format = Zend_Registry::getInstance()->config['format'];
		if ( $format == 'xhtml' || $format == 'html' )
		     $return = ( $oneLined ) 
		             ? str_replace( "\n", " ", str_replace( "\t", " ", Zend_Debug::dump( $var ) ) ) : Zend_Debug::dump( $var );
          else $return = ( $oneLined ) 
		             ? str_replace( "\n", " ", str_replace( "\t", " ", var_export( $var, true ) ) ) : var_export( $var, true );
		
          if ( $return ) return $return;
		else print $return;
	}
}
