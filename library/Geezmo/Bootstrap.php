<?php
/**
 *  This file is part of geezmo-core.
 *  
 *  geezmo-core is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  geezmo-core is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with geezmo-core.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @copyright geezmo-kit
 * @package geezmo
 * @subpackage core
 */

class Geezmo_Bootstrap
{
     public function loadComponent ( $component, $module = NULL )
     {
          if ( is_null( $module ) )
               $fileName = Zend_Registry::getInstance()->environment['path']['bootstrap'] . "$component.php";

          else $fileName = Zend_Registry::getInstance()->environment['path']['modules'] . 
                           DIRECTORY_SEPARATOR . $module . 
                           DIRECTORY_SEPARATOR . 'bootstrap' . 
                           DIRECTORY_SEPARATOR . "$component.php";

          if ( file_exists( $fileName ) )
               require_once $fileName;

          if ( is_null( $module ) && file_exists( $fileName ) )
               Zend_Registry::getInstance()->environment['loadedComponent'][] = $component;
          elseif ( file_exists( $fileName ) ) Zend_Registry::getInstance()->environment['loadedModulesComponent'][$module][] = $component;
     }
     
     public function loadModule ( $moduleName )
     {
          $module = Zend_Registry::getInstance()->environment['path']['modules'] . 
                        DIRECTORY_SEPARATOR . $moduleName;
                        
          // *** ( global ) Set new include path
          if ( file_exists( $module . DIRECTORY_SEPARATOR . 'library' ) )
               set_include_path( get_include_path( ) . PATH_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'library' );

          $uri = explode( '/', $_SERVER['REQUEST_URI'] );

          // *** ( request ) Bootstrap Components
          if ( count( $uri ) >= 3 && $uri[0] == $moduleName )
          {
               foreach ( Zend_Registry::getInstance()->environment['loadedComponent'] as $component )
                      Geezmo_Bootstrap::loadComponent( $component, $moduleName );

               foreach ( glob( $module . DIRECTORY_SEPARATOR . 'bootstrap' . DIRECTORY_SEPARATOR . '*.php' ) as $moduleComponent )
               {
                    if ( ! in_array( substr( basename( $moduleComponent ), 0, -4 ), Zend_Registry::getInstance()->environment['loadedComponent'] ) )
                         Geezmo_Bootstrap::loadComponent( substr( basename( $moduleComponent ), 0, -4 ), $moduleName );
               }
          }

          // *** ( global ) Load Configurations
          foreach ( glob( $module . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . '*.ini' ) as $configFile )
          {
               $configName = preg_replace( '/.ini/', NULL, basename( $configFile ) );

               $confs[$configName] = new Zend_Config_Ini( $configFile,
                    Zend_Registry::getInstance()->environment['type'],
                    TRUE );
          }
     }
     
     public function loadModules ( )
     {
           foreach ( glob( Zend_Registry::getInstance()->environment['path']['modules'] . '*' ) as $module )
           {
                $moduleName = basename( $module );
                Geezmo_Bootstrap::loadModule( $moduleName );
           }
     }
}
